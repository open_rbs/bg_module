package app.com.bgmodule;


import android.app.Activity;
import android.app.Notification;

/**
 * <p>Класс - модуль для работы с фоновым режимом</p>
 * <p>С его помощью приложение остаеться работать на заднем плане, когда его скроют</p>

 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 */
public class BgModule{

    /**
     * Инициализация концигураций для уведомлений
     */
    private static final String ICON = "";
    private static final String TITLE = "";
    private static final String TEXT = "";

    /**
     * Уникальный айди увидомления
     */
    public static final int ID = 1;

    /**
     * Сервис для показа уведомлений
     */
    private BgService service;

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "BACKGROUND_MODULE";

    private Activity activity;

    public BgModule(Activity _activity){
        activity = _activity;
    }

    /**
     * <p>Нужно ли активировать задний фон при скрытие приложения</p>
     */
    private boolean active = false;

    /**
     * <p>Запуск сервиса</p>
     */
    public void startService(){

    }

    /**
     * <p>Остановка сервиса</p>
     */
    public void stopService(){

    }

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    public void destroy() {

    }


    public void activate(){
        active = true;
    }

    public void deactivate(){
        active = false;
    }

    class BgService{
        /**
         * <p>Показывает уведомление</p>
         */
        private void awake(){

        }

        /**
         * <p>Закрывает уведомление</p>
         */
        private void sleep(){

        }

        /**
         * <p>Создает уведомления</p>
         * @return Возвращает уведомление для приложения
         */
        private Notification makeNotification(){
            return null;
        }
    }
}
